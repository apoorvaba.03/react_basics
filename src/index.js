import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import MyTitle from './MyTitle';


//var names = ['Alice','Emily','Kate'];
// props examples start
class Hellomessage extends React.Component{
  render()
  {
    return(
      <div>
        <h1>hello {this.props.name}</h1>
      </div>
    );
  }
}
//props examples stop
// childerns react start
class NotesList extends React.Component{
  render(){
    return(
      <ol>
        {
          React.Children.map(this.props.children,function (child){
            return <li>{child}</li>
          })
        }
      </ol>
    );
  }
}
// childerns stop
var data = 123;
// null example start
function Message({message}){
  return <div>{message}</div>
}
const element = <Message message={null} />;
//stop
// box styling start
const element2 = (
  <div className="box-small">React Box</div>
)
// box styling stop
ReactDOM.render(
  /*<div>                                                   
    {
      names.map(function(name,index)
      {
        return <div key={index}>Helllo,{name}!</div>
      }
      )
    }
  </div>,
  <Hellomessage name="pradeep" />,
  <NotesList>
    <span>hello</span>
    <span>world</span>
  </NotesList>,
  <MyTitle title={data}/>,*/
  <App />,    // components examples 
  //element,
  //element2,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
