import logo from './logo.svg';
import './App.css';
import { Component } from 'react';

function App() {
  return (
    <div className="App">
       <DemoClass democlass="React classes"></DemoClass>
       <Author authorname="yuwen"></Author>
      <CmpStyle studentname="Hello world"></CmpStyle>
    </div>
  );
}

class Author extends Component{
  render(){
    return(
      <h1><u>{this.props.authorname}</u></h1>
    );
  }
}
class DemoClass extends Component{
  render()
  {
    return(
      <h1><u>{this.props.democlass}</u></h1>
    );
  }
}
class CmpStyle extends Component{
  render(){
    var myStyle = {
      color:'Green'
    }
    return(
      <h1 style={myStyle}>Calculation:4-2={4-2}</h1>
    );
  }
}



export default App;
